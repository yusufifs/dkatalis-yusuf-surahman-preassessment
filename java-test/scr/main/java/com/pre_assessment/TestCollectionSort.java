import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestCollectionSort {
	
	public static void main(String[] args) {
		TestCollectionSort.newSort(new ArrayList<>());
	}
	
	private static List<String> newSort(List<String> list) {
		return list.stream().sorted((a,b) -> b.compareTo(a)).collect(Collectors.toList());
	}

}
