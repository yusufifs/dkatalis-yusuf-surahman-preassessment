import React from 'react';

function MyComponent() {
  return (
    <div className="MyComponent">
      <h1>Hello World !!!</h1>
    </div>
  );
}

export default MyComponent;
